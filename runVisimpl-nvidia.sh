#!/bin/bash

xhost +si:localuser:root > /dev/null 2>&1

docker run -it \
--gpus 1 \
--rm \
--env="DISPLAY" \
--volume="/tmp/.X11-unix:/tmp/.X11-unix" \
--volume="/etc/machine-id:/etc/machine-id" \
--privileged visimpl-nvidia-ubuntu-16.04:0.1.4 /usr/bin/startVisimpl.sh

xhost -si:localuser:root > /dev/null 2>&1
