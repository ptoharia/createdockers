#!/bin/bash

set -e

DOCKER_IMAGES=(visimpl neurotessmesh)

declare -A DOCKER_APPS=(
    [visimpl]="visimpl stackviz"
    [neurotessmesh]="neurotessmesh"
)

declare -A APPS_EXECUTABLES=(
    [visimpl]="visimpl.sh"
    [stackviz]="stackviz"
    [neurotessmesh]="NeuroTessMesh"
)

declare -A VERS=(
  [visimpl]=0.1.4
  [neurotessmesh]=0.0.1
)

declare -A EXTRAINSTRUCTIONS=(
  [visimpl]="COPY visimpl.sh /usr/bin/"
  [neurotessmesh]=""
)

for DOCKER_IMAGE in "${DOCKER_IMAGES[@]}"
do
    echo "-- PROCESSING DOCKER IMAGE" ${DOCKER_IMAGE}
    VER=${VERS[$DOCKER_IMAGE]}

    echo "-- Creating Dockerfile-nvidia"

    cat > Dockerfile.${DOCKER_IMAGE}.nvidia-ubuntu-16.04 <<EOF
FROM nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04

ENV NVIDIA_DRIVER_CAPABILITIES \${NVIDIA_DRIVER_CAPABILITIES},display
EOF

    echo ${EXTRAINSTRUCTIONS[$DOCKER_IMAGE]}  >> Dockerfile.${DOCKER_IMAGE}.nvidia-ubuntu-16.04

    cat >> Dockerfile.${DOCKER_IMAGE}.nvidia-ubuntu-16.04 <<EOF
RUN apt-get update && apt-get install -y --no-install-recommends mesa-utils libc6:amd64 libdrm2:amd64 libexpat1:amd64 libffi6:amd64 libfreetype6:amd64 libgcc1:amd64 libgl1-mesa-glx:amd64 libglapi-mesa:amd64 libglib2.0-0:amd64 libgraphite2-3:amd64 libharfbuzz0b:amd64 libice6:amd64 libpcre3:amd64 libsm6:amd64 libstdc++6:amd64 libuuid1:amd64 libx11-6:amd64 libx11-xcb1:amd64 libxau6:amd64 libxcb1:amd64 libxcb-dri2-0:amd64 libxcb-dri3-0:amd64 libxcb-glx0:amd64 libxcb-present0:amd64 libxcb-sync1:amd64 libxdamage1:amd64 libxdmcp6:amd64 libxfixes3:amd64 libxshmfence1:amd64 zlib1g:amd64 libfontconfig1:amd64 xkb-data avahi-daemon && rm -rf /var/lib/apt/lists/*
EOF

    echo "-- Creating Dockerfile-ubuntu"

    cat > Dockerfile.${DOCKER_IMAGE}.ubuntu-16.04 <<EOF
FROM ubuntu:16.04
EOF

    echo ${EXTRAINSTRUCTIONS[$DOCKER_IMAGE]} >> Dockerfile.${DOCKER_IMAGE}.ubuntu-16.04

    cat >> Dockerfile.${DOCKER_IMAGE}.ubuntu-16.04 <<EOF
RUN apt-get update && apt-get install -y --no-install-recommends mesa-utils libc6:amd64 libdrm2:amd64 libexpat1:amd64 libffi6:amd64 libfreetype6:amd64 libgcc1:amd64 libgl1-mesa-glx:amd64 libglapi-mesa:amd64 libglib2.0-0:amd64 libgraphite2-3:amd64 libharfbuzz0b:amd64 libice6:amd64 libpcre3:amd64 libsm6:amd64 libstdc++6:amd64 libuuid1:amd64 libx11-6:amd64 libx11-xcb1:amd64 libxau6:amd64 libxcb1:amd64 libxcb-dri2-0:amd64 libxcb-dri3-0:amd64 libxcb-glx0:amd64 libxcb-present0:amd64 libxcb-sync1:amd64 libxdamage1:amd64 libxdmcp6:amd64 libxfixes3:amd64 libxshmfence1:amd64 zlib1g:amd64 libfontconfig1:amd64 xkb-data avahi-daemon && rm -rf /var/lib/apt/lists/*
EOF

    for APP in ${DOCKER_APPS[$DOCKER_IMAGE]}
    do
        echo "---- Processing App" ${APP}

        APPIMAGE=${APP}-${VER}-x86_64.AppImage
        echo "------ Download AppImage" ${APPIMAGE}
        if [ ! -f "${DOCKER_IMAGE}/${APP}/$APPIMAGE" ]
        then
            echo "Downloading $APPIMAGE"
            mkdir -p ${DOCKER_IMAGE}/${APP}
            cd ${DOCKER_IMAGE}/${APP}
            wget http://gmrv.es/gmrvvis/apps/${DOCKER_IMAGE}/${VER}/${APP}-${VER}-x86_64.AppImage
            cd ../..
        fi

        cd ${DOCKER_IMAGE}/${APP}

        APPIMAGE=${APP}-${VER}-x86_64.AppImage
        chmod +x ${APPIMAGE}

        echo "------ Extracting AppImage" ${APPIMAGE}
        if [ ! -d "squashfs-root" ]
        then
            ./${APPIMAGE} --appimage-extract
        fi

        echo "------ Linking appdir.${APP}" ${APPIMAGE}
        if [ ! -d "appdir.${APP}" ]; then
            ln -s squashfs-root appdir.${APP}
        fi

        cd ../..


        cat >> Dockerfile.${DOCKER_IMAGE}.nvidia-ubuntu-16.04 <<EOF

COPY ${DOCKER_IMAGE}/${APP}/appdir.${APP}/usr /usr/
EOF
        cat >> Dockerfile.${DOCKER_IMAGE}.ubuntu-16.04 <<EOF

COPY ${DOCKER_IMAGE}/${APP}/appdir.${APP}/usr /usr/
EOF

        running_instructions=${running_instructions}"docker run --gpus 1 -ti --rm -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /etc/machine-id:/etc/machine-id --privileged ${DOCKER_IMAGE}-nvidia-ubuntu-16.04:${VER} /usr/bin/${APPS_EXECUTABLES[${APP}]}

"
        running_instructions=${running_instructions}"docker run --gpus 1 -ti --rm -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /etc/machine-id:/etc/machine-id --privileged ${DOCKER_IMAGE}-ubuntu-16.04:${VER} /usr/bin/${APPS_EXECUTABLES[${APP}]}

"

    done # for each app


    echo "---- Building ${DOCKER_IMAGE} ubuntu docker image"
    DOCKER_IMAGE_SUFFIX=nvidia-ubuntu-16.04
    docker build --network=host --tag ${DOCKER_IMAGE}-${DOCKER_IMAGE_SUFFIX}:${VER} \
           -f Dockerfile.${DOCKER_IMAGE}.${DOCKER_IMAGE_SUFFIX} .

    echo "---- Building ${DOCKER_IMAGE} nvidia docker image"
    DOCKER_IMAGE_SUFFIX=ubuntu-16.04
    docker build --network=host --tag ${DOCKER_IMAGE}-${DOCKER_IMAGE_SUFFIX}:${VER} \
           -f Dockerfile.${DOCKER_IMAGE}.${DOCKER_IMAGE_SUFFIX} .


done # for each docker image


echo
echo "* Running instructions:"
echo
echo "$running_instructions"
echo xhost +local:docker > running_instructions.txt
echo "$running_instructions" >> running_instructions.txt


echo "To erase images:" >> running_instructions.txt
echo "docker image rm neurotessmesh-ubuntu-16.04:0.0.1 neurotessmesh-nvidia-ubuntu-16.04:0.0.1  visimpl-ubuntu-16.04:0.1.4  visimpl-nvidia-ubuntu-16.04:0.1.4" >> running_instructions.txt
